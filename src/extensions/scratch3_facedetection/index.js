const ArgumentType = require("../../extension-support/argument-type");
const BlockType = require("../../extension-support/block-type");
const TargetType = require("../../extension-support/target-type");
const StageLayering = require("../../engine/stage-layering");
const Video = require("/node_modules/scratch-vm/src/io/video.js");

const Cast = require("../../util/cast");
const faceapi = require("/node_modules/scratch-vm/node_modules/face-api.js/build/es6/index");

//import * as faceapi from "face-api.js";
//import faceDetectInsetIconURL from '../../../../../src/lib/libraries/extensions/facedetection/face-detect.png';

const blockIconURI =
    "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAABmJLR0QA/wD/AP+gvaeTAAAErElEQVRYw+2YfUwbdRjH9xoX4h8mCwm0ZbTHUd4WN4UIUxEM0yyZynjZMEYH7QYmZiYYNJC9OMZLDNsYXXltWbYoWUSajWkWiWLGa1+YIM6JLmYQ2GDy/lLYtQ6uj8+dBbuulPa46kz8JZ80/V3v+3z69C6/33XNmv/HYzyEQuFrAoHgtFgsfupxlNuGcr8ggNzx8fEJ5S0cANYjKicUI17e3t5PYvFsxICMIJMo1ouv41axJfz8/EbLysp24HmEAza7K7gR/hoUMmlPT0/PnfDw8DyUuWkv4oz09HRYZpzhKnjE/hgWykVod8Rs0Wg0+ZibYQPFm6Cvr+9LWMTCVc5Knl2tad4EMfziKuUYGjwpeJsHwR89KTjNFAkiiYG9u6J1b78RcyPu+WcHw4ICKLxhLH/fsSJLxNMh5oRXdkzLEuPGY6O299tctzc8JigUCozypLjxWV0lUAbVI4w2KWGqrdzhsZLs/YNWwRY+BNchRuQ923mJ/5bx3qunHAqshFFbCWL/LSYUvLBqQeuJmxlR27mgQOIWF7lFtgaTwyh4yK5OJvIcLyuMlJR03GtUcBYkCfGMSCSK9Nj6GkRKvtLXfMxZUCQUzmMHn/GYYCAhrqo9eYiT3HhzKXuHh4WRoR6Rm9eeLSjLfosqPyKjuQj2N5xmOghT14ruQWuJL++CCzrF9a7PcuDciYOcBEeuKeHlqG2AOTCvVb7Kv6C2RGluOwO36gtnuV6DtZ9kMIJmaFV687EnTEZClt4blKEYTpv0lQNcBed1SljQK2r42rSOIQq767DYZKjo4ChoWdArR/CLiuw2xqPIO1wEJ5CzdnNrqeuV+7gJqtuhU+Vll7fJuqTm8CLIzjflbsCCv7oraNKr0xzU4F+QGZS+KtFNwbvwtfKJf0yQ/akNVXpXBe/r1enL5HhGkBlm3TkSixtduvbq6tZ7SrAe2emAaKaLcz99kX3fWecMKurBb9rtTmqsWtDZSAQLPTymrYZZ7aMbWGN7BVBDP5vwc0eRw0inA35YjWDMMt1jiLIWBYtpGsbaKmHku2KYbMEddasShhuLwdjfxVa20At9+MI8ENc5YQ+vy55K1blxnqa7FltpoSZgtkMNv39bBH1XC2G61/BQq4cnZqI8+j9LbGzuBpKUhUoDZCnxSXk51V+2DGm7e7ptJei5UTB9r4IHdx+Wm5ycMuPnjQUVl6oDJbIPpYR8J2atei1eG0jIkwLFsgoMbJSS8g5pgLyTISw4Q1f6eSOlvty00Nc/RC910WIBzadXYGbKuCRHmf+A8trGORSEhJT8m4sZLIS8DamXStIOh0gO+LvhlrtOSsguI7Acu3YfA3V9MyhqGqCz+zbQtAU+yqpmj8W8kAUDAyMwODwBpRe/AUbug+MXwFkeYpQSqXGuPXdIZAdXCGNJSCmAqktNrGi+QgP79hdBfHIepL5bAifwvfpKMyuXeew8BJMHwIXMQbE4bZML23qZxhVBhhejs+DoqVpWxJ7CinrYHX8cXM1ikaRFr9xBQt7uVigSEfE+7EnOhzdTT7JdjIzMBHczGEhJ2useEeQLlwSD/OQCqVRO/BsIBBle/7k/4v8EsFJhGW6XqZQAAAAASUVORK5CYII=";
//const faceDetectInsetIconURL = 'data:image/svg+xml,%3Csvg id="rotate-counter-clockwise" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"%3E%3Cdefs%3E%3Cstyle%3E.cls-1%7Bfill:%233d79cc;%7D.cls-2%7Bfill:%23fff;%7D%3C/style%3E%3C/defs%3E%3Ctitle%3Erotate-counter-clockwise%3C/title%3E%3Cpath class="cls-1" d="M22.68,12.2a1.6,1.6,0,0,1-1.27.63H13.72a1.59,1.59,0,0,1-1.16-2.58l1.12-1.41a4.82,4.82,0,0,0-3.14-.77,4.31,4.31,0,0,0-2,.8,4.25,4.25,0,0,0-1.34,1.73,5.06,5.06,0,0,0,.54,4.62A5.58,5.58,0,0,0,12,17.74h0a2.26,2.26,0,0,1-.16,4.52A10.25,10.25,0,0,1,3.74,18,10.14,10.14,0,0,1,2.25,8.78,9.7,9.7,0,0,1,5.08,4.64,9.92,9.92,0,0,1,9.66,2.5a10.66,10.66,0,0,1,7.72,1.68l1.08-1.35a1.57,1.57,0,0,1,1.24-.6,1.6,1.6,0,0,1,1.54,1.21l1.7,7.37A1.57,1.57,0,0,1,22.68,12.2Z"/%3E%3Cpath class="cls-2" d="M21.38,11.83H13.77a.59.59,0,0,1-.43-1l1.75-2.19a5.9,5.9,0,0,0-4.7-1.58,5.07,5.07,0,0,0-4.11,3.17A6,6,0,0,0,7,15.77a6.51,6.51,0,0,0,5,2.92,1.31,1.31,0,0,1-.08,2.62,9.3,9.3,0,0,1-7.35-3.82A9.16,9.16,0,0,1,3.17,9.12,8.51,8.51,0,0,1,5.71,5.4,8.76,8.76,0,0,1,9.82,3.48a9.71,9.71,0,0,1,7.75,2.07l1.67-2.1a.59.59,0,0,1,1,.21L22,11.08A.59.59,0,0,1,21.38,11.83Z"/%3E%3C/svg%3E';

let bounding_box =
    "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxMDAiIGhlaWdodD0iMTAwIj48cGF0aCBkPSJNOTcgM3Y5NEgzVjNoOTRtMy0zSDB2MTAwaDEwMFYweiIgZmlsbD0iI2NmMCIvPjwvc3ZnPg==";

let faces = [];
let isStage = false;
let drawOnStage = false;
let MakerAttributes = [];
let faceThreshold = 0.5;

//Written By Kanak

class FaceDetection {
    constructor(runtime) {
        this.runtime = runtime;

        this.fullFaceDescriptions = [];
        for (var _i = 0; _i < 10; _i++) {
            this.fullFaceDescriptions.push([]);
        }

        for (let i = 0; i < 100; i++) {
            MakerAttributes[i] = {
                color4f: [Math.random(), Math.random(), Math.random(), 0.7],
                diameter: 3,
            };
        }

        this.faceComparision = [];
        this._skinId = -1;
        this._skin = null;
        this._drawable = -1;

        if (this.faceComparision && this.faceComparision.length) {
            console.log("faceComparision.length", this.faceComparision.length);
            var faceKeys = Object.keys(this.faceComparision);
            console.log("faceKeys", faceKeys);

            for (var faceIndex = 0; faceIndex < faceKeys.length; faceIndex++) {
                this.faceComparision[faceKeys[faceIndex]] =
                    new faceapi.LabeledFaceDescriptors.fromJSON(faceComparision[faceKeys[faceIndex]]);
            }
        }

        // this.runtime.emit("MODEL_LOADING");
        this.modelLoaded = false;
        var self = this;

        //console.log("Model Loading...")

        netModel = new Promise((resolve) => {
            this.runtime.emit("MODEL_LOADING");
            Promise.all(
                    [faceapi.loadSsdMobilenetv1Model("static/models/faceDetection/ssd_mobilenetv1_model-weights_manifest.json"),
                        faceapi.loadFaceLandmarkModel("static/models/faceDetection/face_landmark_68_model-weights_manifest.json"),
                        faceapi.loadFaceExpressionModel("static/models/faceDetection/face_expression_model-weights_manifest.json"),
                        faceapi.loadFaceRecognitionModel("static/models/faceDetection/face_recognition_model-weights_manifest.json"),
                    ])
                .then(() => {
                    this.runtime.renderer.requestSnapshot((data) => {
                        let image = document.createElement("img");

                        image.onload = () => {
                            faceapi.detectAllFaces(image, new faceapi.SsdMobilenetv1Options({ minConfidence: faceThreshold }))
                                .withFaceLandmarks(false)
                                .withFaceDescriptors()
                                .withFaceExpressions()
                                .then((fullFaceDescriptions) => {
                                    self.modelLoaded = true;
                                    faces = fullFaceDescriptions;
                                    isStage = true;
                                    runtime.emit("MODEL_LOADING_FINISHED", true);
                                    resolve("Done");
                                    return "Done";
                                });
                        };
                        image.setAttribute("src", data);
                    });
                })
                .catch((err) => {
                    self.modelLoaded = false;
                    this.runtime.emit("MODEL_LOADING_FINISHED", false);
                });
            this.globalVideoState = "off";
            this.video = this.runtime.ioDevices.video;
            this.runtime.ioDevices.video.disableVideo();
        });

        this.extensionName = "Face Detection";
        this.canvas = document.querySelector("canvas");
        this.penSkinId = this.runtime.renderer.createPenSkin();
        var penDrawableId = this.runtime.renderer.createDrawable(StageLayering.SPRITE_LAYER);
        this.runtime.renderer.updateDrawableProperties(penDrawableId, { skinId: this.penSkinId });
    }

    getInfo() {
        return {
            id: "faceDetection",
            name: "Face Detection",
            blockIconURI: blockIconURI,
            // menuIconURI: menuIconURI,
            color1: "#1363e5",
            color2: "#1363e5",
            blocks: [
                // {
                //     message: "Settings",
                // },
                '---',
                {
                    opcode: "toggleStageVideoFeed",
                    text: "turn [VIDEO_STATE] video on stage with [TRANSPARENCY] % transparency",
                    blockType: BlockType.COMMAND,
                    arguments: {
                        VIDEO_STATE: {
                            type: ArgumentType.STRING,
                            menu: "videoState",
                            defaultValue: "on",
                        },
                        TRANSPARENCY: {
                            type: ArgumentType.NUMBER,
                            defaultValue: "0",
                        },
                    },
                },
                {
                    opcode: "drawBoundingBox",
                    text: "[OPTION] bounding box",
                    blockType: BlockType.COMMAND,
                    arguments: {
                        OPTION: {
                            type: ArgumentType.NUMBER,
                            menu: "drawBox",
                            defaultValue: "1",
                        },
                    },
                },
                {
                    opcode: "setThreshold",
                    text: "set detection threshold to [THRESHOLD]",
                    blockType: BlockType.COMMAND,
                    arguments: {
                        THRESHOLD: {
                            type: ArgumentType.NUMBER,
                            menu: "threshold",
                            defaultValue: "0.5",
                        },
                    },
                },
                // {
                //     message: "Detection",
                // },
                '---',
                {
                    opcode: "analyseImage",
                    text: "analyse image from [FEED]",
                    blockType: BlockType.COMMAND,
                    arguments: {
                        FEED: {
                            type: ArgumentType.NUMBER,
                            menu: "feed",
                            defaultValue: "1",
                        },
                    },
                },
                '---',
                {
                    opcode: "getNumberFaces",
                    text: "get # faces",
                    blockType: BlockType.REPORTER,
                },
                {
                    opcode: "getOption",
                    text: "get expression of face [FACE]",
                    blockType: BlockType.REPORTER,
                    arguments: {
                        FACE: {
                            type: ArgumentType.NUMBER,
                            menu: "faceNumber",
                            defaultValue: "1",
                        },
                    },
                },
                {
                    opcode: "isExpression",
                    text: "is expression of face [FACE] [EXPRESSION]",
                    blockType: BlockType.BOOLEAN,
                    arguments: {
                        FACE: {
                            type: ArgumentType.NUMBER,
                            menu: "faceNumber",
                            defaultValue: "1",
                        },
                        EXPRESSION: {
                            type: ArgumentType.NUMBER,
                            menu: "expression",
                            defaultValue: "4",
                        },
                    },
                },
                '---',
                {
                    opcode: "boxPosition",
                    text: "get [POSITION] of face [FACE]",
                    blockType: BlockType.REPORTER,
                    arguments: {
                        FACE: {
                            type: ArgumentType.NUMBER,
                            menu: "faceNumber",
                            defaultValue: "1",
                        },
                        POSITION: {
                            type: ArgumentType.NUMBER,
                            menu: "facePosition",
                            defaultValue: "1",
                        },
                    },
                },
                {
                    opcode: "faceLandmarksF",
                    text: "get [POSITION] of [LANDMARK] of face [FACE]",
                    blockType: BlockType.REPORTER,
                    arguments: {
                        FACE: {
                            type: ArgumentType.NUMBER,
                            menu: "faceNumber",
                            defaultValue: "1",
                        },
                        LANDMARK: {
                            type: ArgumentType.NUMBER,
                            menu: "faceLandmarks",
                            defaultValue: "1",
                        },
                        POSITION: {
                            type: ArgumentType.NUMBER,
                            menu: "landmarkPosition",
                            defaultValue: "1",
                        },
                    },
                },
                {
                    opcode: "landmarks",
                    text: "get [POSITION] of landmark [LANDMARK] of face [FACE]",
                    blockType: BlockType.REPORTER,
                    arguments: {
                        FACE: {
                            type: ArgumentType.NUMBER,
                            menu: "faceNumber",
                            defaultValue: "1",
                        },
                        LANDMARK: {
                            type: ArgumentType.NUMBER,
                            defaultValue: "1",
                        },
                        POSITION: {
                            type: ArgumentType.NUMBER,
                            menu: "landmarkPosition",
                            defaultValue: "1",
                        },
                    },
                },
                // {
                //     message: "Face Recognition: Training",
                // },
                '---',
                {
                    opcode: "saveImage",
                    text: "add class [FACE] as [NAME] from [FEED]",
                    blockType: BlockType.COMMAND,
                    arguments: {
                        FACE: {
                            type: ArgumentType.NUMBER,
                            menu: "faceNumber",
                            defaultValue: "1",
                        },
                        NAME: {
                            type: ArgumentType.STRING,
                            defaultValue: "Kanak",
                        },
                        FEED: {
                            type: ArgumentType.NUMBER,
                            menu: "feed",
                            defaultValue: "1",
                        },
                    },
                },
                {
                    opcode: "deleteImage",
                    text: "reset class",
                    blockType: BlockType.COMMAND,
                },
                // {
                //     message: "Face Recognition: Testing",
                // },
                '---',
                {
                    opcode: "doFaceMatching",
                    text: "do face matching on [FEED]",
                    blockType: BlockType.COMMAND,
                    arguments: {
                        FEED: {
                            type: ArgumentType.NUMBER,
                            menu: "feed",
                            defaultValue: "1",
                        },
                    },
                },
                {
                    opcode: "isClassDetected",
                    text: "is [FACE] class detected",
                    blockType: BlockType.BOOLEAN,
                    arguments: {
                        FACE: {
                            type: ArgumentType.NUMBER,
                            menu: "faceNumber",
                            defaultValue: "1",
                        },
                    },
                },
                {
                    opcode: "getFace",
                    text: "get class of face [FACE] detected",
                    blockType: BlockType.REPORTER,
                    arguments: {
                        FACE: {
                            type: ArgumentType.NUMBER,
                            menu: "faceNumber",
                            defaultValue: "1",
                        },
                    },
                },
            ],
            menus: {
                videoState: [{
                        text: "off",
                        value: "off",
                    },
                    {
                        text: "on",
                        value: "on",
                    },
                    {
                        text: "on flipped",
                        value: "onFlipped",
                    },
                ],
                faceNumber: {
                    acceptReporters: true,
                    items: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
                },
                expression: {
                    acceptReporters: true,
                    items: [{
                            text: "anrgy",
                            value: "1",
                        },
                        {
                            text: "disgusted",
                            value: "2",
                        },
                        {
                            text: "fear",
                            value: "3",
                        },
                        {
                            text: "happy",
                            value: "4",
                        },
                        {
                            text: "neutral",
                            value: "5",
                        },
                        {
                            text: "sad",
                            value: "6",
                        },
                        {
                            text: "surprised",
                            value: "7",
                        },
                    ],
                },
                facePosition: {
                    items: [{
                            text: "x position",
                            value: "1",
                        },
                        {
                            text: "y position",
                            value: "2",
                        },
                        {
                            text: "width",
                            value: "3",
                        },
                        {
                            text: "height",
                            value: "4",
                        },
                    ],
                },
                feed: {
                    items: [{
                            text: "camera",
                            value: "1",
                        },
                        {
                            text: "stage",
                            value: "2",
                        },
                    ],
                },
                option: {
                    items: [{
                            text: "age",
                            value: "1",
                        },
                        {
                            text: "gender",
                            value: "2",
                        },
                        {
                            text: "expression",
                            value: "3",
                        },
                    ],
                },
                landmarkPosition: {
                    items: [{
                            text: "x position",
                            value: "1",
                        },
                        {
                            text: "y position",
                            value: "2",
                        },
                    ],
                },
                faceLandmarks: {
                    items: [{
                            text: "nose",
                            value: "31",
                        },
                        {
                            text: "chin",
                            value: "9",
                        },
                        {
                            text: "mouth",
                            value: "3",
                        },
                        {
                            text: "left eye",
                            value: "1",
                        },
                        {
                            text: "right eye",
                            value: "2",
                        },
                        {
                            text: "left eyebrows",
                            value: "20",
                        },
                        {
                            text: "right eyebrows",
                            value: "25",
                        },
                    ],
                },
                drawBox: [{
                        text: "show",
                        value: "1",
                    },
                    {
                        text: "hide",
                        value: "2",
                    },
                ],
                threshold: {
                    acceptReporters: true,
                    items: [
                        "0.9",
                        "0.8",
                        "0.7",
                        "0.6",
                        "0.5",
                        "0.4",
                        "0.3",
                        "0.2",
                        "0.1",
                    ],
                },
            },
        };
    }

    //1.
    toggleStageVideoFeed(args) {
        const state = Cast.toString(args.VIDEO_STATE);
        this.globalVideoState = state;
        const transparency = Cast.toNumber(args.TRANSPARENCY);
        this.runtime.ioDevices.video.setPreviewGhost(transparency);
        if (state === "off") {
            this.runtime.ioDevices.video.disableVideo();
            drawOnStage = false;
            this.clearMark();
        } else {
            this.runtime.ioDevices.video.enableVideo().then(this.setupPreview());
            this.globalVideoTransparency = transparency;
            this.runtime.ioDevices.video.mirror = state === "on"; // Mirror if state is ON. Do not mirror if state is ON_FLIPPED.
        }
    }

    //2.
    drawBoundingBox(args) {
        const text = Cast.toString(args.TEXT);
        let self = this;
        if (args.OPTION === "1") {
            drawOnStage = true;
            this.clearMark();

            for (let i = 0; i < faces.length; i++) {
                self.drawMark(
                    faces[i].detection.box.topLeft.x,
                    faces[i].detection.box.topLeft.y,
                    faces[i].detection.box.topRight.x,
                    faces[i].detection.box.topRight.y,
                    faces[i].detection.box.bottomRight.x,
                    faces[i].detection.box.bottomRight.y,
                    faces[i].detection.box.bottomLeft.x,
                    faces[i].detection.box.bottomLeft.y,
                    faces[i].detection.imageDims.width,
                    faces[i].detection.imageDims.height,
                    i
                );
            }
        } else {
            drawOnStage = false;
            this.clearMark();
        }
    }

    clearMark() {
        this.runtime.renderer.penClear(this.penSkinId);
    }

    drawMark(x1, y1, x2, y2, x3, y3, x4, y4, width, height, num) {
        let widthScale = 480 / width;
        let heightScale = 360 / height;
        x1 = x1 * widthScale - (width / 2) * widthScale;
        x2 = x2 * widthScale - (width / 2) * widthScale;
        x3 = x3 * widthScale - (width / 2) * widthScale;
        x4 = x4 * widthScale - (width / 2) * widthScale;
        y1 = (height / 2) * heightScale - y1 * heightScale;
        y2 = (height / 2) * heightScale - y2 * heightScale;
        y3 = (height / 2) * heightScale - y3 * heightScale;
        y4 = (height / 2) * heightScale - y4 * heightScale;
        this.runtime.renderer.penLine(this.penSkinId, MakerAttributes[num], x1, y1, x2, y2);
        this.runtime.renderer.penLine(this.penSkinId, MakerAttributes[num], x2, y2, x3, y3);
        this.runtime.renderer.penLine(this.penSkinId, MakerAttributes[num], x4, y4, x3, y3);
        this.runtime.renderer.penLine(this.penSkinId, MakerAttributes[num], x4, y4, x1, y1);
    }

    //3.
    setThreshold(args) {
        faceThreshold = parseFloat(args.THRESHOLD);
    }

    //4.
    analyseImage(args, util) {
        var thisRef = this;
        var self = this;

        if (this.globalVideoState) {
            if (this.modelLoaded) {
                drawOnStage = true;

                if (args.FEED === "1") {
                    var translatePromise = new Promise(function(resolve) {
                        var canvas = document.createElement("canvas");
                        canvas.width = 480;
                        canvas.height = 360;
                        var ctx = canvas.getContext("2d");

                        var frame = thisRef.runtime.ioDevices.video.getFrame({
                            format: Video.FORMAT_IMAGE_DATA,
                            dimensions: [480, 360],
                        });

                        if (frame === null) {
                            faces = [];
                            resolve("Camera not ready!");
                            return "Camera not ready!";
                        }

                        ctx.putImageData(frame, 0, 0);
                        var image = document.createElement("img");
                        image.src = canvas.toDataURL("image/png");

                        // self.canvas = faceapi.createCanvasFromMedia(self.video.provider.video);
                        // document.body.append(self.canvas);
                        // const displaySize = { width: 480, height: 360 };

                        image.onload = function() {
                            faceapi.detectAllFaces(image, new faceapi.SsdMobilenetv1Options({ minConfidence: faceThreshold }))
                                .withFaceLandmarks(false)
                                .withFaceDescriptors()
                                .withFaceExpressions()
                                .then(function(fullFaceDescriptions) {
                                    faces = fullFaceDescriptions;
                                    console.log(fullFaceDescriptions);

                                    if (drawOnStage) {
                                        self.clearMark();

                                        // const resizedDetections = faceapi.resizeResults(faces[0].detection, displaySize);
                                        // faceapi.draw.drawDetections(self.canvas, resizedDetections);

                                        for (let i = 0; i < faces.length; i++) {
                                            self.drawMark(
                                                faces[i].detection.box.topLeft.x,
                                                faces[i].detection.box.topLeft.y,
                                                faces[i].detection.box.topRight.x,
                                                faces[i].detection.box.topRight.y,
                                                faces[i].detection.box.bottomRight.x,
                                                faces[i].detection.box.bottomRight.y,
                                                faces[i].detection.box.bottomLeft.x,
                                                faces[i].detection.box.bottomLeft.y,
                                                faces[i].detection.imageDims.width,
                                                faces[i].detection.imageDims.height,
                                                i);

                                            let radians = function radians(a12, a22, b1, b2) {
                                                return (Math.atan2(b2 - a22, b1 - a12) % Math.PI);
                                            };

                                            let degrees = function degrees(theta) {
                                                return (theta * 180) / Math.PI;
                                            };

                                            let angle = { roll: void 0, pitch: void 0, yaw: void 0, };
                                            if (!faces[i] || !faces[i].landmarks._positions || faces[i].landmarks._positions.length !== 68)
                                                return angle;
                                            let pt = faces[i].landmarks._positions;

                                            angle.roll = degrees(-radians(pt[36]._x, pt[36]._y, pt[45]._x, pt[45]._y));
                                            angle.pitch = degrees(radians(0, Math.abs(pt[0]._x - pt[30]._x) / pt[30]._x, Math.PI, Math.abs(pt[16]._x - pt[30]._x) / pt[30]._x));
                                            let bottom = pt.reduce(function(prev, cur) { return prev < cur._y ? prev : cur._y; }, Infinity);
                                            let top = pt.reduce(function(prev, cur) { return prev > cur._y ? prev : cur._y; }, -Infinity);
                                            angle.yaw = degrees(Math.PI * (faces[i].landmarks._imgDims._height / (top - bottom) / 1.4 - 1));
                                            console.log(angle);
                                        }
                                    }
                                    isStage = false;
                                    resolve("Done");
                                    return "Done";
                                })
                                .catch(function(err) {
                                    faces = [];
                                    console.log(err);
                                    resolve("Error!");
                                    return "Error!";
                                });
                        };
                    });
                    return translatePromise;
                } else if (args.FEED === "2") {
                    return new Promise(function(resolve) {
                        thisRef.runtime.renderer.requestSnapshot(function(data) {
                            var image = document.createElement("img");
                            image.onload = function() {
                                faceapi.detectAllFaces(image, new faceapi.SsdMobilenetv1Options({ minConfidence: faceThreshold }))
                                    .withFaceLandmarks(false)
                                    .withFaceDescriptors()
                                    .withFaceExpressions()
                                    .then(function(fullFaceDescriptions) {
                                        faces = fullFaceDescriptions;
                                        isStage = true;

                                        if (drawOnStage) {
                                            self.clearMark();

                                            for (let i = 0; i < faces.length; i++) {
                                                self.drawMark(
                                                    faces[i].detection.box.topLeft.x,
                                                    faces[i].detection.box.topLeft.y,
                                                    faces[i].detection.box.topRight.x,
                                                    faces[i].detection.box.topRight.y,
                                                    faces[i].detection.box.bottomRight.x,
                                                    faces[i].detection.box.bottomRight.y,
                                                    faces[i].detection.box.bottomLeft.x,
                                                    faces[i].detection.box.bottomLeft.y,
                                                    faces[i].detection.imageDims.width,
                                                    faces[i].detection.imageDims.height,
                                                    i
                                                );
                                            }
                                        }
                                        resolve("Done");
                                        return "Done";
                                    });
                            };

                            image.setAttribute("src", data);
                        });
                    });
                }
            } else {
                this.runtime.emit("MODEL_LOADING");
                return new Promise(function(resolve) {
                    Promise.all([
                            faceapi.loadSsdMobilenetv1Model("static/models/faceDetection/ssd_mobilenetv1_model-weights_manifest.json"),
                            faceapi.loadFaceLandmarkModel("static/models/faceDetection/face_landmark_68_model-weights_manifest.json"),
                            faceapi.loadFaceExpressionModel("static/models/faceDetection/face_expression_model-weights_manifest.json"),
                            faceapi.loadFaceRecognitionModel("static/models/faceDetection/face_recognition_model-weights_manifest.json"),
                        ])
                        .then(function() {
                            thisRef.runtime.emit("MODEL_LOADING_FINISHED", true);
                            thisRef.modelLoaded = true;

                            if (args.FEED === "1") {
                                var canvas = document.createElement("canvas");
                                canvas.width = 480;
                                canvas.height = 360;
                                var ctx = canvas.getContext("2d");

                                var frame = thisRef.runtime.ioDevices.video.getFrame({
                                    format: Video.FORMAT_IMAGE_DATA,
                                    dimensions: [480, 360],
                                });

                                if (frame === null) {
                                    faces = [];
                                    resolve("Camera not ready!");
                                    return "Camera not ready!";
                                }

                                ctx.putImageData(frame, 0, 0);
                                var image = document.createElement("img");
                                image.src = canvas.toDataURL("image/png");

                                image.onload = function() {
                                    faceapi.detectAllFaces(image, new faceapi.SsdMobilenetv1Options({ minConfidence: faceThreshold, }))
                                        .withFaceLandmarks(false)
                                        .withFaceDescriptors()
                                        .withFaceExpressions()
                                        .then(function(fullFaceDescriptions) {
                                            faces = fullFaceDescriptions;
                                            console.log(faces);
                                            isStage = false;

                                            if (drawOnStage) {
                                                self.clearMark();

                                                for (let i = 0; i < faces.length; i++) {
                                                    self.drawMark(
                                                        faces[i].detection.box.topLeft.x,
                                                        faces[i].detection.box.topLeft.y,
                                                        faces[i].detection.box.topRight.x,
                                                        faces[i].detection.box.topRight.y,
                                                        faces[i].detection.box.bottomRight.x,
                                                        faces[i].detection.box.bottomRight.y,
                                                        faces[i].detection.box.bottomLeft.x,
                                                        faces[i].detection.box.bottomLeft.y,
                                                        faces[i].detection.imageDims.width,
                                                        faces[i].detection.imageDims.height,
                                                        i
                                                    );
                                                }
                                            }

                                            resolve("Done");
                                            return "Done";
                                        })
                                        .catch(function(err) {
                                            faces = [];
                                            resolve("No faces detected");
                                            return "No faces detected";
                                        });
                                };
                            } else if (args.FEED === "2") {
                                thisRef.runtime.renderer.requestSnapshot(
                                    function(data) {
                                        var image = document.createElement("img");

                                        image.onload = function() {
                                            faceapi.detectAllFaces(image, new faceapi.SsdMobilenetv1Options({ minConfidence: faceThreshold, }))
                                                .withFaceLandmarks(false)
                                                .withFaceDescriptors()
                                                .withFaceExpressions()
                                                .then(function(fullFaceDescriptions) {
                                                    console.log(faces);
                                                    faces = fullFaceDescriptions;
                                                    isStage = true;

                                                    if (drawOnStage) {
                                                        self.clearMark();

                                                        for (let i = 0; i < faces.length; i++) {
                                                            self.drawMark(faces[i].detection.box.topLeft.x,
                                                                faces[i].detection.box.topLeft.y,
                                                                faces[i].detection.box.topRight.x,
                                                                faces[i].detection.box.topRight.y,
                                                                faces[i].detection.box.bottomRight.x,
                                                                faces[i].detection.box.bottomRight.y,
                                                                faces[i].detection.box.bottomLeft.x,
                                                                faces[i].detection.box.bottomLeft.y,
                                                                faces[i].detection.imageDims.width,
                                                                faces[i].detection.imageDims.height,
                                                                i);
                                                        }
                                                    }

                                                    resolve("Done");
                                                    return "Done";
                                                });
                                        };

                                        image.setAttribute("src", data);
                                    }
                                );
                            }
                        })
                        .catch(function(err) {
                            thisRef.runtime.emit(
                                "MODEL_LOADING_FINISHED",
                                false
                            );

                            resolve("NULL");
                        });
                });
            }
        } else {
            console.log("Camera Not Ready!");
            return "Camera Not Ready!";
        }
    }

    setupPreview() {
        var thisRef = this;
        this.video = this.runtime.ioDevices.video;
        // console.log(this.video); //kanak
        var renderer = this.runtime.renderer;
        if (!renderer) return;

        if (
            this._skinId === -1 &&
            this._skin === null &&
            this._drawable === -1
        ) {
            this._skinId = renderer.createPenSkin();
            this._skin = renderer._allSkins[this._skinId];
            this._drawable = renderer.createDrawable(StageLayering.SPRITE_LAYER);
            renderer.updateDrawableProperties(this._drawable, {
                skinId: this._skinId,
            });
        }

        if (!this.renderPreviewFrame) {
            renderer.updateDrawableProperties(this._drawable, {
                ghost: this._forceTransparentPreview ? 100 : this._ghost,
                visible: true,
            });

            this.renderPreviewFrame = function() {
                clearTimeout(thisRef._renderPreviewTimeout);

                if (!thisRef.renderPreviewFrame) {
                    return;
                }

                thisRef._renderPreviewTimeout = setTimeout(
                    thisRef.renderPreviewFrame,
                    thisRef.runtime.currentStepTime
                );

                var canvas = thisRef.getFrame({
                    format: Video.FORMAT_CANVAS,
                });

                if (!canvas) {
                    thisRef._skin.clear();
                    return;
                }

                var xOffset = Video.DIMENSIONS[0] / -2;
                var yOffset = Video.DIMENSIONS[1] / 2;

                //_this2._skin.drawStamp(canvas, xOffset, yOffset);

                var ctx = canvas.getContext("2d");
                ctx.drawImage(canvas, xOffset + xOffset, yOffset - yOffset);
                // this._canvasDirty = true;
                // this._silhouetteDirty = true;
                thisRef.runtime.requestRedraw();
            };

            this.renderPreviewFrame();
        }
    }

    getFrame(ref) {
        var refdimensions = ref.dimensions,
            dimensions = refdimensions === void 0 ? Video.DIMENSIONS : refdimensions,

            refmirror = ref.mirror,
            mirror = refmirror === void 0 ? this.mirror : refmirror,

            refformat = ref.format,
            format = refformat === void 0 ? Video.FORMAT_IMAGE_DATA : refformat,

            refcacheTimeout = ref.cacheTimeout,
            cacheTimeout = refcacheTimeout === void 0 ? this._frameCacheTimeout : refcacheTimeout;

        if (this.runtime.ioDevices.video)
            return this.runtime.ioDevices.video.getFrame({
                dimensions: dimensions,
                mirror: mirror,
                format: format,
                cacheTimeout: cacheTimeout,
            });

        return null;
    }

    //5.
    getNumberFaces(args, util) {
        return faces.length;
    }

    //6.
    getOption(args, util) {
        if (faces[parseInt(args.FACE, 10) - 1]) {
            var expressionValue = 0;
            var expression;

            if (faces[parseInt(args.FACE, 10) - 1].expressions.angry > expressionValue) {
                expressionValue = faces[parseInt(args.FACE, 10) - 1].expressions.angry;
                expression = "angry";
            }

            if (faces[parseInt(args.FACE, 10) - 1].expressions.disgusted > expressionValue) {
                expressionValue = faces[parseInt(args.FACE, 10) - 1].expressions.disgusted;
                expression = "disgusted";
            }

            if (faces[parseInt(args.FACE, 10) - 1].expressions.fearful > expressionValue) {
                expressionValue = faces[parseInt(args.FACE, 10) - 1].expressions.fearful;
                expression = "fear";
            }

            if (faces[parseInt(args.FACE, 10) - 1].expressions.happy > expressionValue) {
                expressionValue = faces[parseInt(args.FACE, 10) - 1].expressions.happy;
                expression = "happy";
            }

            if (faces[parseInt(args.FACE, 10) - 1].expressions.neutral > expressionValue) {
                expressionValue = faces[parseInt(args.FACE, 10) - 1].expressions.neutral;
                expression = "neutral";
            }

            if (faces[parseInt(args.FACE, 10) - 1].expressions.sad > expressionValue) {
                expressionValue = faces[parseInt(args.FACE, 10) - 1].expressions.sad;
                expression = "sad";
            }

            if (faces[parseInt(args.FACE, 10) - 1].expressions.surprised > expressionValue) {
                expressionValue = faces[parseInt(args.FACE, 10) - 1].expressions.surprised;
                expression = "surprised";
            }

            return expression;
        } else {
            return "NULL";
        }
    }

    //7.
    isExpression(args, util) {
        if (faces[parseInt(args.FACE, 10) - 1]) {
            var expressionValue = 0;
            var expression;

            if (faces[parseInt(args.FACE, 10) - 1].expressions.angry > expressionValue) {
                expressionValue = faces[parseInt(args.FACE, 10) - 1].expressions.angry;
                expression = "1";
            }

            if (faces[parseInt(args.FACE, 10) - 1].expressions.disgusted > expressionValue) {
                expressionValue = faces[parseInt(args.FACE, 10) - 1].expressions.disgusted;
                expression = "2";
            }

            if (faces[parseInt(args.FACE, 10) - 1].expressions.fearful > expressionValue) {
                expressionValue = faces[parseInt(args.FACE, 10) - 1].expressions.fearful;
                expression = "3";
            }

            if (faces[parseInt(args.FACE, 10) - 1].expressions.happy > expressionValue) {
                expressionValue = faces[parseInt(args.FACE, 10) - 1].expressions.happy;
                expression = "4";
            }

            if (faces[parseInt(args.FACE, 10) - 1].expressions.neutral > expressionValue) {
                expressionValue = faces[parseInt(args.FACE, 10) - 1].expressions.neutral;
                expression = "5";
            }

            if (faces[parseInt(args.FACE, 10) - 1].expressions.sad > expressionValue) {
                expressionValue = faces[parseInt(args.FACE, 10) - 1].expressions.sad;
                expression = "6";
            }

            if (faces[parseInt(args.FACE, 10) - 1].expressions.surprised > expressionValue) {
                expressionValue = faces[parseInt(args.FACE, 10) - 1].expressions.surprised;
                expression = "7";
            }

            if (args.EXPRESSION === expression) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    //8.
    boxPosition(args, util) {
        return "Coming Soon";
    }

    //9.
    landmarks(args, util) {
        return "Coming Soon";
    }

    //10.
    faceLandmarksF(args, util) {
        return "Coming Soon";
    }

    //11.
    saveImage(args, util) {
        return "Coming Soon";
    }

    //12.
    doFaceMatching(args, util) {
        return "Coming Soon";
    }

    //13.
    deleteImage(args, util) {
        return "Coming Soon";
    }

    //14.
    isClassDetected(args, util) {
        return "Coming Soon";
    }

    //15.
    getFace(args, util) {
        return "Coming Soon";
    }
}

module.exports = FaceDetection;